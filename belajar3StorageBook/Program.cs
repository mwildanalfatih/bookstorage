﻿using System;
using System.Collections.Generic;

namespace bookstorage
{
    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library();
            for (int i = 1; i <= 1000; i++)
            {
                Book newBook = new Book
                {
                    Title = $"Book {i}",
                    Id = i,
                    Description = $"Description of Book {i}",
                    Year = 2021
                };
                library.AddBook(newBook);
            }

            string command = "";
            do
            {
                Console.WriteLine("Enter command (list, program, exit):");
                 command = Console.ReadLine();

                switch (command.ToLower())
                {
                    case "list":
                        List<Book> books = library.GetBooks();
                        foreach (Book book in books)
                        {
                            Console.WriteLine($"{book.Id}. {book.Title} ({book.Year})\n{book.Description}\n");
                        }
                        break;

                    case "program":
                        Random random = new Random();
                        List<Book> programbooks = library.GetBooks();

                        Task[] editTasks = new Task[50];
                        for (int i = 0; i < 50; i++)
                        {
                            editTasks[i] = Task.Run(() =>
                            {
                                Random random = new Random();
                                int randomIndex = random.Next(programbooks.Count);
                                Book randomBook = programbooks[randomIndex];
                                string newTitle = $"New Title {randomBook.Id} - Book Title was Edited ";
                                library.EditBook(randomBook.Id, newTitle, randomBook.Description, randomBook.Year);
                                Console.WriteLine($"Edited book ID {randomBook.Id}. New Title: {newTitle}");

                                for (int j = 0; j < 1000; j++)
                                {
                                    newTitle = $"New Title {randomBook.Id} - Book Title was Edited {j + 1} times";
                                    try
                                    {
                                        library.EditBook(randomBook.Id, newTitle, randomBook.Description, randomBook.Year);
                                        Console.WriteLine($"Edited book ID {randomBook.Id}. New Title: {newTitle} {j + 1} times");
                                    }
                                    catch (ArgumentException ex)
                                    {
                                        Console.WriteLine(ex.Message);
                                    }
                                }
                            });
                        }

                        
                        Task[] getTasks = new Task[50];
                        for (int i = 0; i < 50; i++)
                        {
                            getTasks[i] = Task.Run(() =>
                            {
                            Random random = new Random();
                            int randomIndex = random.Next(programbooks.Count);
                            Book randomBook = programbooks[randomIndex];

                                for (int j = 0; j < 1000; j++)
                                {
                                    try
                                    {
                                        Book result = library.GetBook(randomBook.Id);
                                        Console.WriteLine($"Get book ID {randomBook.Id}. Title: {result.Title}, Description: {result.Description}, Year: {result.Year}, get {j + 1} times");
                                    }
                                    catch (ArgumentException ex)
                                    {
                                        Console.WriteLine(ex.Message);
                                    }
                                }
                            });
                        }

                        Task[] addTasks = new Task[50];
                        for (int i = 0; i < 50; i++)
                        {
                            addTasks[i] = Task.Run(() =>
                            {
                                for (int j = 0; j < 1000; j++)
                                {
                                    string newTitle = $"New Book {Guid.NewGuid().ToString()}";
                                    string newDescription = $"Description for {newTitle}";
                                    int newYear = random.Next(2021, 2022);
                                    int newId = random.Next(1, 10);
                                    try
                                    {
                                        library.AddBook(new Book { Id = newId, Title = newTitle, Description = newDescription, Year = newYear });
                                        Console.WriteLine($"Added book with Title: {newTitle}");
                                    }
                                    catch (ArgumentException ex)
                                    {
                                        Console.WriteLine(ex.Message);
                                    }
                                }
                            });
                        }

                        Task[] removeTasks = new Task[50];
                        for (int i = 0; i < 50; i++)
                        {
                            removeTasks[i] = Task.Run(() =>
                            {
                                for (int j = 0; j < 1000; j++)
                                {
                                    Random random = new Random();
                                    int maxId = programbooks.Max(book => book.Id);
                                    int randomIndex = random.Next(maxId);
                                    try
                                    {
                                        library.RemoveBook(randomIndex);
                                        Console.WriteLine($"Book with ID = {randomIndex} removed successfully.");
                                    }
                                    catch (ArgumentException ex)
                                    {
                                        Console.WriteLine(ex.Message);
                                    }
                                }
                            });
                        }

                        Task.WaitAll(addTasks);
                        Task.WaitAll(editTasks);
                        Task.WaitAll(getTasks);
                        Task.WaitAll(removeTasks);
                        break;

                    case "exit":
                        Console.WriteLine("Goodbye!");
                        return;

                    default:
                        Console.WriteLine("Command not recognized.");
                        break;
                }
            } while (command.ToLower() != "exit");
        }
    }
}