﻿using System;
using System.Collections.Generic;

public class Book
{
    public string Title { get; set; }
    public int Id { get; set; }
    public string Description { get; set; }
    public int Year { get; set; }
}
