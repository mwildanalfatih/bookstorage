﻿using System;
using System.Collections.Generic;

public class Library
{
    private List<Book> books = new List<Book>();
    public void AddBook(Book book)
    {
        if (book == null)
        {
            throw new ArgumentNullException(nameof(book));
        }
        if (string.IsNullOrEmpty(book.Title))
        {
            throw new ArgumentException("Title cannot be null or empty.", nameof(book.Title));
        }
        if (book.Id <= 0)
        {
            throw new ArgumentException("Id must be greater than zero.", nameof(book.Id));
        }
        if (books.Count >= 50000)
        {
            throw new InvalidOperationException("Cannot add more book.");
        }

        var booksCopy = new List<Book>(books);
        int newId = booksCopy.Any() ? booksCopy.Max(b => b.Id) + 1 : 1;
        while (booksCopy.Any(b => b.Id == newId))
        {
            newId++;
        }
        book.Id = newId;
        booksCopy.Add(book);
        books = booksCopy;
    }


    public void EditBook(int id, string newTitle, string newDescription, int newYear)
    {
        Book book = books.FirstOrDefault(book => book.Id == id);
        if (book == null)
        {
            throw new ArgumentException($"Cant edit, Book with ID = {id} not found.", nameof(id));
        }
        book.Title = newTitle;
        book.Description = newDescription;
        book.Year = newYear;
    }
    public List<Book> GetBooks()
    {
        return books;
    }
    public Book GetBook(int id)
    {
        Book book = books.Find(book => book.Id == id);
        if (book == null)
        {
            throw new ArgumentException($"No book found with ID {id}.", nameof(id));
        }
        return book;
    }
    public void RemoveBook(int id)
    {
        List<Book> tempBooks = new List<Book>(books);
        Book bookToRemove = tempBooks.SingleOrDefault(book => book.Id == id);
        if (bookToRemove != null)
        {
            tempBooks.Remove(bookToRemove);
        }
        else
        {
            throw new ArgumentException($"cant delete, No book found with ID {id}.", nameof(id));
        }
        books = tempBooks;
    }
}
